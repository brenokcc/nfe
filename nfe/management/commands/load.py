# -*- coding: utf-8 -*-
from os.path import abspath, dirname, join
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        base_dir = abspath(dirname(dirname(dirname(__file__))))
        files = ['localidades.json', 'cfop.json', 'icms.json', 'ncm.json']
        for file_name in files:
            call_command('loaddata', join(base_dir, 'fixtures/%s' % file_name))
