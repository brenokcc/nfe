# -*- coding: utf-8 -*-
import json
import base64
import re
import StringIO
from django.http.response import HttpResponseRedirect, HttpResponse
from django.views.decorators import csrf
from nfe.models import *
from nfe.forms import *
from djangoplus.decorators.views import view, action


@view(u'Home', login_required=False, add_menu=False)
def home(request):
    return locals()

@action(NotaFiscal, u'Pré-Visualizar', perm_or_group='nfe.add_lote', style='popup', category='Visualizar', condition='not protocolo')
def visualizar_nota_xml(request, pk):
    nota = NotaFiscal.objects.get(pk=pk)
    title = unicode(nota)
    xml = nota.to_xml()
    return locals()


@action(NotaFiscal, u'Visualizar', style='blank', condition='protocolo', category='Visualizar')
def download_xml(request, pk):
    nota = NotaFiscal.objects.get(pk=pk)
    url = '/media/nfe/%s/%s.nfe.xml' % (nota.emitente.pk, nota.numero)
    return HttpResponseRedirect(url)


@action(NotaFiscal, u'Consultar', style='popup', condition='chave', category=None, template='consultar.html')
def consultar_nota(request, pk):
    title = u'Consulta de Nota - SEFAZ'
    nota = NotaFiscal.objects.get(pk=pk)
    json = nota.consultar()
    return locals()


@action(Emitente, u'Consultar Status', style='popup', condition='caminho_certificado', category=None, template='consultar.html')
def consultar_emitente(request, pk):
    title = u'Consultar Status'
    emitente = Emitente.objects.get(pk=pk)
    json = emitente.consultar()
    return locals()



#curl --data  'token=356a192b7913b04c54574d18c28d46e6395428ab&json={} http://localhost:8000/fabrica/webservice/

@view('Webservice', login_required=False)
def webservice(request):
    s = ''

    method = request.POST.get('method')
    token = request.POST.get('token')

    if method == 'registrar':
        cnpj = request.POST.get('cnpj')
        certificado = request.POST.get('certificado')
        senha = request.POST.get('senha')
        qs = Emitente.objects.filter(cnpj=cnpj, ie=None)
        if qs.exists():
            emitente = qs[0]
        else:
            emitente = Emitente()
            emitente.cnpj = cnpj
            emitente.save()
        emitente.upload_certificado(StringIO.StringIO(base64.b64decode(certificado)), senha)
        d = dict(token=emitente.token)
    else:
        emitente = Emitente.objects.get(token=token)
        if method == 'enviar':
            xml = request.POST.get('xml')
            numero = re.findall(re.compile('<nNF>[0-9]+</nNF>'), xml)[0][5:-6].strip()
            qs = NotaFiscal.objects.filter(emitente=emitente, numero=numero)
            if qs.exists():
                nota = qs[0]
            else:
                nota = NotaFiscal()
                nota.emitente = emitente
                nota.numero = numero
                nota.finalizada = True
                nota.save()
            nota.enviar()
            d = dict(chave=nota.chave)
        elif method == 'cancelar':
            chave = request.POST.get('chave')
            motivo = request.POST.get('motivo')
            nota = NotaFiscal.objects.get(chave=chave)
            nota.cancelar(motivo)
            d = dict(status=nota.codigo_situacao, motivo=nota.situacao)
        elif method == 'baixar':
            chave = request.POST.get('chave')
            nota = NotaFiscal.objects.get(chave=chave)
            xml = open(nota.get_path(True), 'r').read()
            d = dict(xml=xml)
        elif method == 'cadastro':
            cnpj = request.POST.get('cnpj')
            pessoa_juridica = emitente.consultar_cadastro(cnpj)
            d = {}
            if pessoa_juridica:
                d['nome'] = pessoa_juridica.nome
                d['nome_fantasia'] = pessoa_juridica.nome_fantasia
                d['logradouro'] = pessoa_juridica.logradouro
                d['numero'] = pessoa_juridica.numero
                d['bairro'] = pessoa_juridica.bairro
                d['ie'] = pessoa_juridica.ie
                d['municipio'] = pessoa_juridica.municipio.codigo
                d['cep'] = pessoa_juridica.cep
        elif method == 'consultar':
            d = []
            chaves = request.POST.get('chaves').split()
            for nota in NotaFiscal.objects.filter(chave__in=chaves):
                d.append(dict(chave=nota.chave, status=nota.codigo_situacao, motivo=nota.situacao))
    return HttpResponse(json.dumps(d))
