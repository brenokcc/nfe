# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-12-07 13:39
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('admin', '0008_auto_20161124_1825'),
    ]

    operations = [
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cpf', djangoplus.db.models.fields.CpfField(max_length=255, verbose_name='CPF')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name=b'Nome')),
                ('email', djangoplus.db.models.fields.CharField(blank=True, default=b'', max_length=255, verbose_name=b'E-mail')),
                ('telefone_principal', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Principal')),
                ('telefone_secundario', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Secund\xe1rio')),
            ],
            options={
                'verbose_name': 'Administrador',
                'verbose_name_plural': 'Administradores',
            },
        ),
        migrations.CreateModel(
            name='CEST',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
            ],
            options={
                'verbose_name': 'CEST',
                'verbose_name_plural': 'CEST',
            },
        ),
        migrations.CreateModel(
            name='CFOP',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
            ],
            options={
                'verbose_name': 'CFOP',
                'verbose_name_plural': 'CFOP',
            },
        ),
        migrations.CreateModel(
            name='Emitente',
            fields=[
                ('organization_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='admin.Organization')),
                ('cnpj', djangoplus.db.models.fields.CnpjField(max_length=255, null=True, verbose_name='CNPJ')),
                ('razao_social', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Raz\xe3o Social')),
                ('nome_fantazia', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Nome Fantasia')),
                ('ie', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Inscri\xe7\xe3o Estadual')),
                ('crt', djangoplus.db.models.fields.IntegerField(choices=[[1, b'Simples Nacional'], [2, b'Simples Nacional \xe2\x80\x93 excesso de sublimite de receita bruta'], [3, b'Regime Normal']], null=True, verbose_name='C\xf3digo de Regime Tribut\xe1rio')),
                ('logradouro', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Rua/Avenida')),
                ('numero', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='N\xfamero')),
                ('complemento', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Complemento')),
                ('bairro', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Bairro')),
                ('cep', djangoplus.db.models.fields.CepField(max_length=255, null=True, verbose_name='CEP')),
                ('telefone_principal', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Principal')),
                ('telefone_secundario', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Secund\xe1rio')),
                ('email', djangoplus.db.models.fields.EmailField(blank=True, max_length=255, null=True, verbose_name='E-mail')),
                ('natureza_operacao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Natureza da Opera\xe7\xe3o')),
                ('modelo_documento', djangoplus.db.models.fields.IntegerField(choices=[[55, 'NF-e'], [65, 'NFC-e']], null=True, verbose_name='Modelo do Documento Fiscal')),
                ('serie', djangoplus.db.models.fields.CharField(help_text='S\xe9rie normal 0-889, Avulsa Fisco 890-899, SCAN 900-999', max_length=255, null=True, verbose_name='S\xe9rie do Documento Fiscal')),
                ('identificacao_destino', djangoplus.db.models.fields.IntegerField(choices=[[1, 'Interna'], [2, 'Interestadual'], [3, 'Exterior']], help_text='Identifica\xe7\xe3o do Local de destino da opera\xe7\xe3o', null=True, verbose_name='Destino')),
                ('tipo_impressao', djangoplus.db.models.fields.IntegerField(choices=[[0, 'Sem DANFE'], [1, 'DANFe Retrato'], [2, 'DANFe Paisagem'], [3, 'DANFe Simplificado'], [4, 'DANFe NFC-e'], [5, 'DANFe NFC-e em mensagem eletr\xf4nica']], null=True, verbose_name='Formato de impress\xe3o do DANFE')),
                ('tipo_emissao', djangoplus.db.models.fields.IntegerField(choices=[[1, 'Normal'], [2, 'Conting\xeancia FS'], [3, 'Conting\xeancia SCAN'], [4, 'Conting\xeancia DPEC'], [5, 'Conting\xeancia FSDA'], [6, 'Conting\xeancia SVC - AN'], [7, 'Conting\xeancia SVC - RS'], [9, 'Conting\xeancia off-line NFC-e']], null=True, verbose_name='Forma de emiss\xe3o')),
                ('tipo_ambiente', djangoplus.db.models.fields.IntegerField(choices=[[1, 'Produ\xe7\xe3o'], [2, 'Homologa\xe7\xe3o']], null=True, verbose_name='Identifica\xe7\xe3o do Ambiente')),
                ('finalidade', djangoplus.db.models.fields.IntegerField(choices=[[1, 'NFe normal'], [2, 'NFe complementar'], [3, 'NFe de ajuste'], [4, 'Devolu\xe7\xe3o/Retorno']], help_text='Finalidade da emiss\xe3o da NF-e', null=True, verbose_name='Finalidade')),
                ('consumidor_final', djangoplus.db.models.fields.IntegerField(choices=[[0, 'N\xe3o'], [1, 'Sim']], help_text='Indica opera\xe7\xe3o com consumidor final', null=True, verbose_name='Consumidor Final')),
                ('presencial', djangoplus.db.models.fields.IntegerField(choices=[[0, 'N\xe3o se aplica (Ex.: Nota Fiscal complementar ou de ajuste)'], [1, 'Opera\xe7\xe3o presencial'], [2, 'N\xe3o presencial, internet'], [3, 'N\xe3o presencial, teleatendimento'], [4, 'NFC - e entrega em domic\xedlio'], [9, 'N\xe3o presencial,outros']], help_text='Indicador de presen\xe7a do comprador no estabelecimento comercial no momento da oepra\xe7\xe3o', null=True, verbose_name='Tipo de Opera\xe7\xe3o')),
                ('procedimento', djangoplus.db.models.fields.IntegerField(choices=[[0, 'Emiss\xe3o de NF-e com aplicativo do contribuinte']], help_text='Processo de emiss\xe3o utilizado com a seguinte codifica\xe7\xe3o', null=True, verbose_name='Processo de Emiss\xe3o')),
                ('destino', djangoplus.db.models.fields.IntegerField(choices=[[1, 'Interna'], [2, b'Interestadual'], [3, b'Exterior']], null=True, verbose_name='Destino da Opera\xe7\xe3o')),
                ('versao', djangoplus.db.models.fields.CharField(help_text='Vers\xe3o do aplicativo utilizado no processo de emiss\xe3o', max_length=255, null=True, verbose_name='Vers\xe3o do Aplicativo')),
                ('caminho_certificado', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Caminho do Certificado')),
                ('sequencial', djangoplus.db.models.fields.IntegerField(default=0, verbose_name='N\xfamero da \xdaltima Nota')),
                ('token', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Token')),
            ],
            options={
                'verbose_name': 'Emitente',
                'verbose_name_plural': 'Emitentes',
            },
            bases=('admin.organization',),
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('sigla', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Sigla')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
            ],
            options={
                'verbose_name': 'Estado',
                'verbose_name_plural': 'Estados',
            },
        ),
        migrations.CreateModel(
            name='Funcionario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cpf', djangoplus.db.models.fields.CpfField(max_length=255, verbose_name='CPF')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name=b'Nome')),
                ('email', djangoplus.db.models.fields.CharField(blank=True, default=b'', max_length=255, verbose_name=b'E-mail')),
                ('telefone_principal', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Principal')),
                ('telefone_secundario', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Secund\xe1rio')),
                ('emitente', djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Emitente', verbose_name='Emitente')),
            ],
            options={
                'verbose_name': 'Funcion\xe1rio',
                'verbose_name_plural': 'Funcion\xe1rios',
            },
        ),
        migrations.CreateModel(
            name='ICMS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
                ('explicacao', djangoplus.db.models.fields.TextField(verbose_name='Explica\xe7\xe3o')),
            ],
            options={
                'verbose_name': 'ICMS',
                'verbose_name_plural': 'ICMS',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantidade', djangoplus.db.models.fields.DecimalField(decimal_places=2, max_digits=9, verbose_name='Quantidade')),
                ('compoe_valor_total', djangoplus.db.models.fields.BooleanField(default=True, verbose_name='Comp\xf5e Valor Total')),
                ('valor', djangoplus.db.models.fields.DecimalField(decimal_places=2, max_digits=9, verbose_name='Valor')),
                ('total', djangoplus.db.models.fields.DecimalField(decimal_places=2, max_digits=9, verbose_name='Total')),
            ],
            options={
                'verbose_name': 'Item',
                'verbose_name_plural': 'Itens',
            },
        ),
        migrations.CreateModel(
            name='Lote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', djangoplus.db.models.fields.IntegerField(verbose_name='N\xfamero')),
                ('data_envio', djangoplus.db.models.fields.DateTimeField(default=datetime.datetime.now, verbose_name='Data do Envio')),
                ('resposta', djangoplus.db.models.fields.TextField(null=True, verbose_name='Resposta')),
                ('protocolo', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Protocolo')),
                ('resultado', djangoplus.db.models.fields.TextField(null=True, verbose_name='Resultado')),
                ('emitente', djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Emitente', verbose_name='Emitente')),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Lote',
                'verbose_name_plural': 'Lotes',
            },
        ),
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
                ('estado', djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Estado', verbose_name='Estado')),
            ],
            options={
                'verbose_name': 'Munic\xedpio',
                'verbose_name_plural': 'Munic\xedpios',
            },
        ),
        migrations.CreateModel(
            name='NCM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
            ],
            options={
                'verbose_name': 'NCM',
                'verbose_name_plural': 'NCM',
            },
        ),
        migrations.CreateModel(
            name='NotaFiscal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', djangoplus.db.models.fields.IntegerField(verbose_name='N\xfamero')),
                ('tipo', djangoplus.db.models.fields.IntegerField(choices=[[0, 'Entrada'], [1, 'Sa\xedda']], null=True, verbose_name='Tipo')),
                ('tipo_ie_destinatario', djangoplus.db.models.fields.IntegerField(choices=[[1, 'Contribuinte ICMSpagamento \xe0 vista'], [2, 'Contribuinte isento de inscri\xe7\xe3o'], [9, 'N\xe3o Contribuinte']], null=True, verbose_name='Tipo de IE do destinat\xe1rio')),
                ('forma_pagamento', djangoplus.db.models.fields.IntegerField(choices=[[0, 'Pagamento \xe0 vista'], [1, 'Pagamento \xe0 prazo'], [2, 'Outros']], null=True, verbose_name='Forma de pagamento')),
                ('data_emissao', djangoplus.db.models.fields.DateTimeField(null=True, verbose_name='Data da Emiss\xe3o')),
                ('data_movimentacao', djangoplus.db.models.fields.DateTimeField(default=datetime.datetime.now, help_text='Data e Hora da sa\xedda ou de entrada da mercadoria/produto.', null=True, verbose_name='Data da Movimenta\xe7\xe3o')),
                ('chave', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Chave de Acesso')),
                ('protocolo', djangoplus.db.models.fields.TextField(null=True, verbose_name='Protocolo')),
                ('situacao', djangoplus.db.models.fields.TextField(null=True, verbose_name='Situa\xe7\xe3o')),
                ('codigo_situacao', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='C\xf3digo da Situa\xe7\xe3o')),
                ('motivo_cancelamento', djangoplus.db.models.fields.TextField(null=True, verbose_name='Motivo')),
                ('protocolo_cancelamento', djangoplus.db.models.fields.TextField(null=True, verbose_name='Protocolo de Cancelamento')),
                ('finalizada', djangoplus.db.models.fields.BooleanField(default=False, verbose_name='Conclu\xedda')),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name': 'Nota Fiscal',
                'verbose_name_plural': 'Notas Fiscais',
            },
        ),
        migrations.CreateModel(
            name='Pais',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
            ],
            options={
                'verbose_name': 'Pa\xeds',
                'verbose_name_plural': 'Paises',
            },
        ),
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('documento', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='CPF/CNPJ')),
                ('logradouro', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Rua/Avenida')),
                ('numero', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='N\xfamero')),
                ('complemento', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Complemento')),
                ('bairro', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Bairro')),
                ('cep', djangoplus.db.models.fields.CepField(max_length=255, null=True, verbose_name='CEP')),
                ('telefone_principal', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Principal')),
                ('telefone_secundario', djangoplus.db.models.fields.PhoneField(blank=True, max_length=255, null=True, verbose_name='Telefone Secund\xe1rio')),
                ('email', djangoplus.db.models.fields.EmailField(blank=True, max_length=255, null=True, verbose_name='E-mail')),
            ],
            options={
                'verbose_name': 'Pessoa',
                'verbose_name_plural': 'Pessoas',
            },
        ),
        migrations.CreateModel(
            name='Produto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='C\xf3digo')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
                ('valor', djangoplus.db.models.fields.MoneyField(decimal_places=2, max_digits=9, verbose_name='Valor')),
                ('cest', djangoplus.db.models.fields.CharField(blank=True, help_text='Codigo especificador da Substuicao Tributaria - CEST, que identifica a mercadoria sujeita aos regimes de  substituicao tribut\xe1ria e de antecipa\xe7\xe3o do recolhimento  do imposto', max_length=255, null=True, verbose_name='CEST')),
                ('cfop', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='CFOP')),
                ('origem_mercadoria', djangoplus.db.models.fields.IntegerField(choices=[[0, b'Nacional'], [1, b'Estrangeira - Importa\xc3\xa7\xc3\xa3o direta'], [2, b'Estrangeira - Adquirida no mercado interno']], default=0, verbose_name='Origem da Mercadoria')),
                ('emitente', djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Emitente', verbose_name='Emitente')),
                ('ncm', djangoplus.db.models.fields.ModelChoiceField(help_text='Nomeclatura Comum do Mercosul', on_delete=django.db.models.deletion.CASCADE, to='nfe.NCM', verbose_name='NCM')),
            ],
            options={
                'verbose_name': 'Produto',
                'verbose_name_plural': 'Produtos',
            },
        ),
        migrations.CreateModel(
            name='UnidadeMedida',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
                ('emitente', djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Emitente', verbose_name='Emitente')),
            ],
            options={
                'verbose_name': 'Unidade de Medida',
                'verbose_name_plural': 'Unidades de Medida',
            },
        ),
        migrations.CreateModel(
            name='PessoaFisica',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='nfe.Pessoa')),
                ('cpf', djangoplus.db.models.fields.CpfField(max_length=255, verbose_name='CPF')),
            ],
            options={
                'verbose_name': 'Pessoa F\xedsica',
                'verbose_name_plural': 'Pessoas F\xedsicas',
            },
            bases=('nfe.pessoa',),
        ),
        migrations.CreateModel(
            name='PessoaJuridica',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='nfe.Pessoa')),
                ('nome_fantasia', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Nome Fantasia')),
                ('cnpj', djangoplus.db.models.fields.CnpjField(max_length=255, verbose_name='CNPJ')),
                ('ie', djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Inscri\xe7\xe3o Estadual')),
            ],
            options={
                'verbose_name': 'Pessoa Jur\xeddica',
                'verbose_name_plural': 'Pessoas Jur\xeddicas',
            },
            bases=('nfe.pessoa',),
        ),
        migrations.AddField(
            model_name='produto',
            name='unidade_medida',
            field=djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.UnidadeMedida', verbose_name='Unidade de Medida'),
        ),
        migrations.AddField(
            model_name='pessoa',
            name='emitente',
            field=djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Emitente', verbose_name='Emitente'),
        ),
        migrations.AddField(
            model_name='pessoa',
            name='municipio',
            field=djangoplus.db.models.fields.ModelChoiceField(null=True, on_delete=django.db.models.deletion.CASCADE, to='nfe.Municipio', verbose_name='Cidade'),
        ),
        migrations.AddField(
            model_name='notafiscal',
            name='destinatario',
            field=djangoplus.db.models.fields.ModelChoiceField(null=True, on_delete=django.db.models.deletion.CASCADE, to='nfe.Pessoa', verbose_name='Destinat\xe1rio'),
        ),
        migrations.AddField(
            model_name='notafiscal',
            name='emitente',
            field=djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Emitente', verbose_name='Emitente'),
        ),
        migrations.AddField(
            model_name='lote',
            name='notas',
            field=djangoplus.db.models.fields.MultipleModelChoiceField(to='nfe.NotaFiscal', verbose_name='Notas'),
        ),
        migrations.AddField(
            model_name='item',
            name='nota',
            field=djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.NotaFiscal', verbose_name='Nota Fiscal'),
        ),
        migrations.AddField(
            model_name='item',
            name='produto',
            field=djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='nfe.Produto', verbose_name='Produto'),
        ),
        migrations.AddField(
            model_name='estado',
            name='pais',
            field=djangoplus.db.models.fields.ModelChoiceField(default=1, null=True, on_delete=django.db.models.deletion.CASCADE, to='nfe.Pais', verbose_name='Pa\xeds'),
        ),
        migrations.AddField(
            model_name='emitente',
            name='municipio',
            field=djangoplus.db.models.fields.ModelChoiceField(null=True, on_delete=django.db.models.deletion.CASCADE, to='nfe.Municipio', verbose_name='Cidade'),
        ),
    ]
