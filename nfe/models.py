# -*- coding: utf-8 -*-
import time
from threading import Thread
from django.conf import settings
from djangoplus.db import models
import socket, json, datetime, hashlib, os
from django.db.models.aggregates import Sum
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string
from djangoplus.admin.models import Organization
from djangoplus.decorators import attr, action, subset

JAR_DIR = '/var/opt/sefaz'
JAR_FILE = '/var/opt/sefaz/nfe.jar'


class Pais(models.Model):

    nome = models.CharField(u'Nome', search=True)
    codigo = models.CharField(u'Código', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', 'codigo')}),
    )

    class Meta:
        verbose_name = u'País'
        verbose_name_plural = u'Paises'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return u'%s' % self.nome


class Estado(models.Model):

    nome = models.CharField(u'Nome', search=True)
    sigla = models.CharField(u'Sigla', search=True)
    codigo = models.CharField(u'Código', search=True)
    pais = models.ForeignKey(Pais, verbose_name=u'País', null=True, blank=False, default=1)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', 'sigla', 'codigo', 'pais')}),
    )

    class Meta:
        verbose_name = u'Estado'
        verbose_name_plural = u'Estados'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return u'%s' % self.sigla


class Municipio(models.Model):
    estado = models.ForeignKey(Estado, verbose_name=u'Estado')
    nome = models.CharField(verbose_name=u'Nome', search=True)
    codigo = models.CharField(u'Código', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('estado', 'nome', 'codigo')}),
    )

    class Meta:
        verbose_name = u'Município'
        verbose_name_plural = u'Municípios'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return u'%s/%s' % (self.nome, self.estado)


class NCM(models.Model):
    descricao = models.CharField(verbose_name=u'Descrição', search=True)
    codigo = models.CharField(verbose_name=u'Código', search=True)

    class Meta:
        verbose_name = u'NCM'
        verbose_name_plural = u'NCM'
        verbose_female = True
        domain = u'Cadastros::Fiscal', 'fa-th'
        list_per_page = 200

    def __unicode__(self):
        return u'%s - %s' % (self.codigo, self.descricao)


class ICMS(models.Model):

    codigo = models.CharField(u'Código', search=True)
    descricao = models.CharField(u'Descrição')
    explicacao = models.TextField(u'Explicação')

    fieldsets = (
        (u'Dados Gerais', {'fields': (('codigo', 'descricao'), 'explicacao'), }),
    )

    class Meta:
        verbose_name = u'ICMS'
        verbose_name_plural = u'ICMS'
        domain = u'Cadastros::Fiscal', 'fa-th'

    def __unicode__(self):
        return u'%s - %s' % (self.codigo, self.descricao)


class CFOP(models.Model):

    codigo = models.CharField(u'Código', search=True)
    descricao = models.CharField(u'Descrição')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('codigo', 'descricao',), }),
    )

    class Meta:
        verbose_name = u'CFOP'
        verbose_name_plural = u'CFOP'
        domain = u'Cadastros::Fiscal', 'fa-th'

    def __unicode__(self):
        return u'%s - %s' % (self.codigo, self.descricao)


class CEST(models.Model):

    codigo = models.CharField(u'Código', search=True)
    descricao = models.CharField(u'Descrição')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('codigo', 'descricao',), }),
    )

    class Meta:
        verbose_name = u'CEST'
        verbose_name_plural = u'CEST'
        domain = u'Cadastros::Fiscal', 'fa-th'

    def __unicode__(self):
        return u'%s - %s' % (self.codigo, self.descricao)


class Administrador(models.Model):
    cpf = models.CpfField(verbose_name=u'CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField(verbose_name=u'Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name=u'Telefone Secundário', null=True, blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': (('cpf', 'nome'), 'email')}),
        (u'Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = u'Administrador'
        verbose_name_plural = u'Administradores'
        role_username = 'cpf'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return u'%s' % self.nome


class Emitente(Organization):

    cnpj = models.CnpjField(verbose_name=u'CNPJ', null=True, blank=False)
    razao_social = models.CharField(verbose_name=u'Razão Social', search=True, null=True)
    nome_fantazia = models.CharField(verbose_name=u'Nome Fantasia', search=False, null=True)
    ie = models.CharField(verbose_name=u'Inscrição Estadual', null=True, blank=False)
    crt = models.IntegerField(verbose_name=u'Código de Regime Tributário', null=True, blank=False, choices=[[1, 'Simples Nacional'], [2, 'Simples Nacional – excesso de sublimite de receita bruta'],  [3,  'Regime Normal']])

    logradouro = models.CharField(verbose_name=u'Rua/Avenida', null=True, blank=False)
    numero = models.CharField(verbose_name=u'Número', null=True, blank=False)
    complemento = models.CharField(verbose_name=u'Complemento', null=True, blank=True)
    bairro = models.CharField(verbose_name=u'Bairro', null=True, blank=False)
    cep = models.CepField(verbose_name=u'CEP', null=True, blank=False)
    municipio = models.ForeignKey(Municipio, verbose_name=u'Cidade', null=True, blank=False, lazy=True)

    telefone_principal = models.PhoneField(verbose_name=u'Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name=u'Telefone Secundário', null=True, blank=True)
    email = models.EmailField(verbose_name=u'E-mail', blank=True, null=True)

    caminho_certificado = models.CharField(verbose_name=u'Caminho do Certificado', exclude=True, null=True)

    versao = models.CharField(verbose_name=u'Versão do Aplicativo', null=True, help_text=u'Versão do aplicativo utilizado no processo de emissão')

    sequencial = models.IntegerField(verbose_name=u'Número da Última Nota', default=0)
    token = models.CharField(verbose_name=u'Token', null=True, exclude=True)

    fieldsets = (
        (u'Dados do Emitente', {'fields': (('cnpj', 'razao_social'), ('nome_fantazia', 'ie'), 'crt')}),
        (u'Endereço do Emitente', {'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
        (u'Contato do Emitente', {'fields': (('telefone_principal', 'telefone_secundario'), 'email')}),
        (u'Dados da Emissão', {'fields': (('versao', 'sequencial'), 'caminho_certificado')}),
        (u'Funcionários', {'relations': ('funcionario_set',)}),
    )

    class Meta:
        verbose_name = u'Emitente'
        verbose_name_plural = u'Emitentes'
        domain = u'Cadastros', 'fa-th'
        add_message = u'Informe os funcionários que emitirão as notas fiscais.'
        can_admin = u'Administrador'
        list_shortcut = True
        icon = 'fa-building'
        list_display = 'cnpj', 'razao_social', 'token'

    def __unicode__(self):
        return '%s (%s)' % (self.razao_social, self.cnpj)

    def get_caminho_diretorio(self):
        return '%s/media/nfe/%s/' % (settings.BASE_DIR, self.pk)

    def consultar(self):
        return execute('status %s' % self.caminho_certificado)

    @action(u'Upload Certificado', input='UploadCertificadoForm')
    def upload_certificado(self, arquivo, senha):
        root_dir = self.get_caminho_diretorio()
        if not os.path.exists(root_dir):
            os.mkdir(root_dir)
        os.system('rm %s*.pfx' % root_dir)
        caminho_certificado = '%s%s.pfx' % (root_dir, senha)
        open(caminho_certificado, 'w').write(arquivo.read())
        self.caminho_certificado = caminho_certificado
        self.save()

    def save(self, *args, **kwargs):
        if not self.token:
            sha1 = hashlib.sha1()
            sha1.update('%s' % datetime.datetime.now())
            self.token = sha1.hexdigest()
        super(Emitente, self).save(*args, **kwargs)

    def consultar_cadastro(self, cnpj):
        qs = PessoaJuridica.objects.filter(cnpj=cnpj)
        if qs.exists():
            return qs[0]
        else:
            comando = 'cadastro %s %s' % (self.caminho_certificado, cnpj)
            json = execute(comando)
            if json:
                pessoa_juridica = PessoaJuridica()
                pessoa_juridica.emitente = self
                pessoa_juridica.nome = json['razao_social']
                pessoa_juridica.nome_fantasia = json['nome_fantazia']
                pessoa_juridica.ie = json['ie']
                pessoa_juridica.logradouro = json['endereco']
                pessoa_juridica.numero = json['numero']
                pessoa_juridica.bairro = json['bairro']
                pessoa_juridica.cep = '%s.%s-%s' % (json['cep'][0:2], json['cep'][2:5], json['cep'][5:])
                pessoa_juridica.complemento = 'complemento' in json and json['complemento'] or ''
                pessoa_juridica.municipio = Municipio.objects.get(codigo=json['municipio'])
                pessoa_juridica.save()
                return pessoa_juridica
            return None


class Funcionario(models.Model):
    emitente = models.ForeignKey(Emitente, verbose_name=u'Emitente')
    cpf = models.CpfField(verbose_name=u'CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField(verbose_name=u'Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name=u'Telefone Secundário', null=True, blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('emitente', ('cpf', 'nome'), 'email')}),
        (u'Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = u'Funcionário'
        verbose_name_plural = u'Funcionários'
        role_username = 'cpf'
        role_scope = 'emitente'
        auxiliar = True
        can_admin = u'Administrador'

    def __unicode__(self):
        return u'%s' % self.nome


class Pessoa(models.Model):
    emitente = models.ForeignKey(Emitente, verbose_name=u'Emitente')
    nome = models.CharField(u'Nome', null=False, blank=False, search=True)
    documento = models.CharField(u'CPF/CNPJ', null=False, blank=False, search=True, exclude=True)

    logradouro = models.CharField(verbose_name=u'Rua/Avenida', null=True, blank=False)
    numero = models.CharField(verbose_name=u'Número', null=True, blank=False)
    complemento = models.CharField(verbose_name=u'Complemento', null=True, blank=True)
    bairro = models.CharField(verbose_name=u'Bairro', null=True, blank=False)
    cep = models.CepField(verbose_name=u'CEP', null=True, blank=False)
    municipio = models.ForeignKey(Municipio, verbose_name=u'Cidade', null=True, blank=False, lazy=True)

    telefone_principal = models.PhoneField(verbose_name=u'Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name=u'Telefone Secundário', null=True, blank=True)
    email = models.EmailField(verbose_name=u'E-mail', blank=True, null=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('emitente', 'nome', 'documento')}),
        (u'Endereço',{'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
        (u'Contato', {'fields': (('telefone_principal', 'telefone_secundario'), 'email')}),
    )

    class Meta:
        verbose_name = u'Pessoa'
        verbose_name_plural = u'Pessoas'
        auxiliar = True

    def __unicode__(self):
        return u'%s'%self.nome


class PessoaFisica(Pessoa):

    cpf = models.CpfField(u'CPF', null=False, blank=False, search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('emitente', ('nome', 'cpf'),)}),
        (u'Endereço', {'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
        (u'Contato', {'fields': (('telefone_principal', 'telefone_secundario'), 'email')}),
    )

    class Meta:
        verbose_name = u'Pessoa Física'
        verbose_name_plural = u'Pessoas Físicas'
        icon = 'fa-user'
        can_admin = u'Funcionário'
        lookups = 'emitente'
        list_display = 'cpf', 'nome'

    def __unicode__(self):
        return u'%s'% self.nome

    def save(self, *args, **kwargs):
        self.documento = self.cpf
        super(PessoaFisica, self).save(*args, **kwargs)


class PessoaJuridica(Pessoa):
    nome_fantasia = models.CharField(verbose_name=u'Nome Fantasia', null=True, search=True)
    cnpj = models.CnpjField(u'CNPJ', null=False, blank=False, search=True)

    ie = models.CharField(verbose_name=u'Inscrição Estadual', null=True, blank=False)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('emitente', 'nome', ('nome_fantasia', 'cnpj')  ,)}),
        (u'Inscrição Estadual', {'fields': (('ie',),)}),
        (u'Endereço', {'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
        (u'Contato', {'fields': (('telefone_principal', 'telefone_secundario'), 'email')}),
    )

    class Meta:
        verbose_name = u'Pessoa Jurídica'
        verbose_name_plural = u'Pessoas Jurídicas'
        icon = 'fa-building'
        can_admin = u'Funcionário'
        lookups = 'emitente'
        list_display = 'cnpj', 'nome_fantasia'

    def __unicode__(self):
        return u'%s' % self.nome_fantasia

    def save(self, *args, **kwargs):
        self.documento = self.cnpj
        super(PessoaJuridica, self).save(*args, **kwargs)


class UnidadeMedida(models.Model):
    emitente = models.ForeignKey(Emitente, verbose_name=u'Emitente')
    descricao = models.CharField(u'Descrição', null=False, blank=False, search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('emitente', 'descricao',), }),
    )

    class Meta:
        verbose_name = u'Unidade de Medida'
        verbose_name_plural = u'Unidades de Medida'
        icon = u'fa-balance-scale'
        domain = u'Cadastros', 'fa-th'
        lookups = 'emitente'
        can_admin = u'Funcionário'

    def __unicode__(self):
        return u'%s' % self.descricao


class Produto(models.Model):
    emitente = models.ForeignKey(Emitente, verbose_name=u'Emitente')
    codigo = models.CharField(u'Código', search=True)
    descricao = models.CharField(u'Descrição', search=True)
    unidade_medida = models.ForeignKey(UnidadeMedida, verbose_name=u'Unidade de Medida')
    valor = models.MoneyField(verbose_name=u'Valor')

    ncm = models.ForeignKey(NCM, verbose_name=u'NCM', lazy=True, help_text=u'Nomeclatura Comum do Mercosul')
    cest = models.CharField(u'CEST', null=True, blank=True, help_text=u'Codigo especificador da Substuicao Tributaria - CEST, que identifica a mercadoria sujeita aos regimes de  substituicao tributária e de antecipação do recolhimento  do imposto')
    cfop = models.CharField(u'CFOP', null=True, blank=True)
    origem_mercadoria = models.IntegerField(verbose_name=u'Origem da Mercadoria', default=0, choices=[[0, 'Nacional'], [1, 'Estrangeira - Importação direta'], [2, 'Estrangeira - Adquirida no mercado interno']])

    fieldsets = (
        (u'Dados Gerais', {'fields': ('emitente', ('codigo', 'descricao'), ('unidade_medida', 'valor')), }),
        (u'Dados Tributários', {'fields': (('ncm', 'cest'), ('cfop', 'origem_mercadoria')), }),
    )

    class Meta:
        verbose_name = u'Produto'
        verbose_name_plural = u'Produtos'
        icon = 'fa-th-large'
        list_shortcut = True
        can_admin = u'Funcionário'
        lookups = 'emitente'

    def __unicode__(self):
        return u'%s' % self.descricao


class NotaFiscalManager(models.Manager):

    @subset(u'Não-Enviadas')
    def nao_enviadas(self):
        return self.filter(chave__isnull=True)

    @subset(u'Não-Autorizadas')
    def nao_autorizada(self):
        return self.filter(protocolo__isnull=True)

    @subset(u'Não-Finalizadas', alert=True)
    def nao_finalizada(self):
        return self.filter(finalizada=False)

    @subset(u'Rejeitadas', alert=True)
    def rejeitadas(self):
        return self.filter(chave__isnull=False, protocolo__isnull=True, situacao__isnull=False)


class NotaFiscal(models.Model):
    emitente = models.ForeignKey(Emitente, verbose_name=u'Emitente', filter=True)

    natureza_operacao = models.CharField(verbose_name=u'Natureza da Operação')
    modelo_documento = models.IntegerField(verbose_name=u'Modelo do Documento Fiscal', null=True, choices=[[55, u'NF-e'], [65, u'NFC-e']])
    serie = models.CharField(verbose_name=u'Série do Documento Fiscal', null=True, help_text=u'Série normal 0-889, Avulsa Fisco 890-899, SCAN 900-999')
    identificacao_destino = models.IntegerField(verbose_name=u'Destino', null=True, help_text=u'Identificação do Local de destino da operação', choices=[[1, u'Interna'], [2, u'Interestadual'], [3, u'Exterior']])

    tipo_impressao = models.IntegerField(verbose_name=u'Formato de impressão do DANFE', null=True, choices=[[0, u'Sem DANFE'], [1, u'DANFe Retrato'], [2, u'DANFe Paisagem'], [3, u'DANFe Simplificado'], [4, u'DANFe NFC-e'], [5, u'DANFe NFC-e em mensagem eletrônica']])
    tipo_emissao = models.IntegerField(verbose_name=u'Forma de emissão', null=True, choices=[[1, u'Normal'], [2, u'Contingência FS'], [3, u'Contingência SCAN'], [4, u'Contingência DPEC'], [5, u'Contingência FSDA'], [6, u'Contingência SVC - AN'], [7, u'Contingência SVC - RS'], [9, u'Contingência off-line NFC-e']])
    tipo_ambiente = models.IntegerField(verbose_name=u'Identificação do Ambiente', null=True, choices=[[1, u'Produção'], [2, u'Homologação']])
    finalidade = models.IntegerField(verbose_name=u'Finalidade', help_text=u'Finalidade da emissão da NF-e', null=True, choices=[[1, u'NFe normal'], [2, u'NFe complementar'], [3, u'NFe de ajuste'], [4, u'Devolução/Retorno']])
    consumidor_final = models.IntegerField(verbose_name=u'Consumidor Final', help_text=u'Indica operação com consumidor final', null=True, choices=[[0, u'Não'], [1, u'Sim']])
    presencial = models.IntegerField(verbose_name=u'Tipo de Operação', help_text=u'Indicador de presença do comprador no estabelecimento comercial no momento da oepração', null=True, choices=[[0, u'Não se aplica (Ex.: Nota Fiscal complementar ou de ajuste)'], [1, u'Operação presencial'], [2, u'Não presencial, internet'], [3, u'Não presencial, teleatendimento'], [4, u'NFC - e entrega em domicílio'], [9, u'Não presencial,outros']])
    procedimento = models.IntegerField(verbose_name=u'Processo de Emissão', help_text=u'Processo de emissão utilizado com a seguinte codificação', null=True, choices=[[0, u'Emissão de NF-e com aplicativo do contribuinte']])

    destino = models.IntegerField(verbose_name=u'Destino da Operação', null=True, choices=[[1, u'Interna'], [2, 'Interestadual'], [3, 'Exterior']])

    numero = models.IntegerField(verbose_name=u'Número', search=True, template_filter='fill8', exclude=True)
    tipo = models.IntegerField(verbose_name=u'Tipo', null=True, choices=[[0, u'Entrada'], [1, u'Saída']])
    tipo_ie_destinatario = models.IntegerField(verbose_name=u'Tipo de IE do destinatário', null=True, choices=[[1, u'Contribuinte ICMSpagamento à vista'], [2, u'Contribuinte isento de inscrição'], [9, u'Não Contribuinte']])
    forma_pagamento = models.IntegerField(verbose_name=u'Forma de pagamento', null=True, choices=[[0, u'Pagamento à vista'], [1, u'Pagamento à prazo'], [2, u'Outros']])

    data_emissao = models.DateTimeField(verbose_name=u'Data da Emissão', null=True, exclude=False)
    data_movimentacao = models.DateTimeField(verbose_name=u'Data da Movimentação', default=datetime.datetime.now, help_text=u'Data e Hora da saída ou de entrada da mercadoria/produto.', exclude=True, null=True)

    chave = models.CharField(verbose_name=u'Chave de Acesso', null=True, exclude=True)

    destinatario = models.ForeignKey(Pessoa, verbose_name=u'Destinatário', null=True)
    protocolo = models.TextField(verbose_name=u'Protocolo', null=True, exclude=True)
    situacao = models.TextField(verbose_name=u'Situação', null=True, exclude=True)
    codigo_situacao = models.CharField(verbose_name=u'Código da Situação', null=True, exclude=True)

    motivo_cancelamento= models.TextField(verbose_name=u'Motivo', null=True, exclude=True)
    protocolo_cancelamento = models.TextField(verbose_name=u'Protocolo de Cancelamento', null=True, exclude=True)
    finalizada = models.BooleanField(verbose_name=u'Concluída', exclude=True, default=False)

    fieldsets = ((u'Dados Gerais', {'fields': (('emitente', 'numero'), 'natureza_operacao', ('tipo', 'destinatario'), ('data_emissao', 'destino'), 'data_movimentacao', ('forma_pagamento', 'tipo_ie_destinatario'), ('tipo_impressao', 'natureza_operacao'), ('modelo_documento',), ('serie', 'identificacao_destino'), ('tipo_emissao', 'tipo_ambiente'), ('finalidade', 'consumidor_final'), ('procedimento', 'presencial'), 'finalizada')}),
        (u'Dados da SEFAZ', {'fields': ('chave', ('situacao', 'protocolo'))}),
        (u'Dados do Cancelamento/Inutilização', {'fields': (('motivo_cancelamento', 'protocolo_cancelamento'),), 'condition' : 'motivo_cancelamento'}),
        (u'Itens', {'relations': ('item_set',)}),
    )

    objects = NotaFiscalManager()

    class Meta:
        icon = 'fa-list-alt'
        verbose_name = u'Nota Fiscal'
        verbose_name_plural = u'Notas Fiscais'
        add_message = u'Por favor, adicione os itens da nota.'
        list_display = 'numero', 'tipo', 'data_emissao', 'destinatario', 'situacao', 'protocolo'
        add_shortcut = True
        list_shortcut = True
        add_label = u'Emitir Nota Fiscal'
        can_admin = u'Funcionário'
        lookups = 'emitente'
        ordering = '-id',

    def initial(self):
        return dict(tipo=1, tipo_ie_destinatario=1, forma_pagamento=0, data_emissao=datetime.datetime.now(), data_movimentacao=datetime.datetime.now(), crt=1, natureza_operacao=u'Venda de mercadoria', modelo_documento=55, serie=1, identificacao_destino=1, tipo_impressao=1, tipo_emissao=1, tipo_ambiente=2, finalidade=1, consumidor_final=0, presencial=0, procedimento=0, destino=1, versao=u'1.0')

    def __unicode__(self):
        return u'Nota %s' % ("%09d" % (self.numero,))

    def can_edit(self):
        return not self.protocolo

    def can_delete(self):
        return not self.protocolo

    def to_xml(self):
        return render_to_string('nota.html', dict(self=self))

    def get_path(self, enviada=False):
        extra = ''
        if enviada:
            extra = '.nfe'
        return '%s%s%s.xml' % (self.emitente.get_caminho_diretorio(), self.numero, extra)

    def store(self, xml=None):
        open(self.get_path(), 'w').write((xml or self.to_xml()).encode('utf8'))

    def get_total(self):
        return self.item_set.aggregate(Sum('total')).get('total__sum') or 0

    def pode_ser_finalizada(self):
        return not self.finalizada and self.item_set.exists() and not self.emitente.notafiscal_set.filter(numero__lt=self.numero, finalizada=False).exists()

    @action(u'Finalizar', condition='pode_ser_finalizada', category=None, style='btn-info ajax')
    def finalizar(self):
        self.finalizada = True
        self.save()

    def pode_ser_enviada(self):
        return self.finalizada and not self.protocolo

    @action(u'Enviar', u'Administrador', category=None, style='btn-success ajax', condition='pode_ser_enviada')
    def enviar(self):
        numero = Lote.objects.all().count() + 1
        lote = Lote(emitente=self.emitente, numero=numero)
        lote.save()
        lote.notas = NotaFiscal.objects.filter(pk=self.pk)
        lote.save()
        comando = ['envia', self.emitente.caminho_certificado, str(lote.numero)]
        self.store()
        comando.append('%s' % self.get_path())
        json = execute(' '.join(comando))
        lote.resposta = unicode(json)
        lote.protocolo = json['recibo']
        lote.save()
        self.chave = json['notas'][0]['chave']
        self.save()
        Thread(target=syncronize()).start()

    def pode_ser_cancelada(self):
        return self.protocolo and not self.motivo_cancelamento

    @action(u'Cancelar', condition='pode_ser_cancelada', category='Cancelamento', style='btn-danger popup')
    def cancelar(self, motivo_cancelamento):
        self.motivo_cancelamento = motivo_cancelamento
        self.save()
        comando = 'cancela %s %s %s' % (self.emitente.caminho_certificado, self.chave, motivo_cancelamento)
        json = execute(comando)
        for retorno in json['retornos']:
            if retorno['status'] != 135:
                self.motivo_cancelamento = None
                self.save()
                raise ValidationError('%s : %s' % (retorno['status'], retorno['motivo']))
            else:
                self.protocolo_cancelamento = retorno['protocolo']
                self.save()
        self.consultar()

    def save(self, *args, **kwargs):
        if not self.numero:
            self.emitente.sequencial += 1
            self.emitente.save()
            self.numero = self.emitente.sequencial
        super(NotaFiscal, self).save(*args, **kwargs)

    def consultar(self):
        comando = 'consulta %s %s' % (self.emitente.caminho_certificado, self.chave)
        json = execute(comando)
        self.situacao = json['motivo']
        self.codigo_situacao = json['status']
        self.protocolo = json['protocolo'] or None
        self.save()
        return json


class Item(models.Model):

    nota = models.ForeignKey(NotaFiscal, verbose_name=u'Nota Fiscal')
    produto = models.ForeignKey(Produto, verbose_name=u'Produto')
    quantidade = models.DecimalField(verbose_name=u'Quantidade')
    compoe_valor_total = models.BooleanField(verbose_name=u'Compõe Valor Total', default=True)

    valor = models.DecimalField(verbose_name=u'Valor', exclude=True)
    total = models.DecimalField(verbose_name=u'Total', exclude=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nota', 'produto', 'valor', 'quantidade', 'compoe_valor_total', 'total')}),
    )

    class Meta:
        verbose_name = u'Item'
        verbose_name_plural = u'Itens'
        domain = u'Cadastros', 'fa-th'
        auxiliar = True
        list_display = 'produto', 'valor', 'quantidade', 'compoe_valor_total', 'total'
        can_admin = u'Funcionário'

    def __unicode__(self):
        return u'Item %s' % self.pk

    def save(self, *args, **kwargs):
        self.valor = self.produto.valor
        self.total = self.valor * self.quantidade
        super(Item, self).save(*args, **kwargs)

    def can_add(self):
        return not self.nota.finalizada

    def can_edit(self):
        return self.can_add()

    def can_delete(self):
        return self.can_add()


class LoteManager(models.Manager):

    @subset(u'Não-Consultados')
    def nao_consultados(self):
        return self.filter(resultado__isnull=True, protocolo__isnull=False)


class Lote(models.Model):
    emitente = models.ForeignKey(Emitente, verbose_name=u'Emitente', exclude=True, filter=True)
    numero = models.IntegerField(u'Número', search=True, exclude=True)
    notas = models.ManyToManyField(NotaFiscal, verbose_name=u'Notas', exclude=True)
    data_envio = models.DateTimeField(verbose_name=u'Data do Envio', default=datetime.datetime.now, exclude=True, filter=True)
    resposta = models.TextField(u'Resposta', null=True, exclude=True)
    protocolo = models.CharField(verbose_name=u'Protocolo', null=True)
    resultado = models.TextField(verbose_name=u'Resultado', null=True, exclude=True)

    fieldsets = (
        (u'Dados do Envio', {'fields': ('emitente', ('numero', 'data_envio')), 'relations':('notas',)}),
        (u'Dados do Processamento', {'fields': ('protocolo', 'resposta', 'resultado'),}),
    )

    class Meta:
        verbose_name = u'Lote'
        verbose_name_plural = u'Lotes'
        list_display = 'data_envio', 'get_informacao'
        can_list = u'Administrador'
        list_shortcut = True
        icon = 'fa-paper-plane'
        lookups = 'emitente'
        ordering = '-pk',

    objects = LoteManager()

    def __unicode__(self):
        return u'Lote %s' % self.numero

    @attr(u'Informação')
    def get_informacao(self):
        if self.resultado:
            return '%s<br/>%s<br/>%s' % (self.protocolo, self.resposta, self.resultado)
        else:
            return '%s<br/>%s' % (self.protocolo, self.resposta)

    def pode_ser_consultado(self):
        return self.protocolo and not self.resultado

    @action(u'Processar')
    def processar(self):
        comando = 'consulta %s %s' % (self.emitente.caminho_certificado, self.protocolo)
        json = execute(comando)
        self.resultado = unicode(json)
        self.save()
        if json['status'] == '104':
            for nota in json['notas']:
                if nota['status'] == '100':
                    self.notas.filter(chave=nota['chave']).update(protocolo=nota['protocolo'], situacao=nota['motivo'], codigo_situacao=nota['status'])
                else:
                    self.notas.filter(chave=nota['chave']).update(situacao=nota['motivo'], codigo_situacao=nota['status'])


def syncronize():
    time.sleep(10)
    while Lote.objects.filter(protocolo__isnull=False, resultado__isnull=True).exists():
        print 'Processando lotes...'
        for lote in Lote.objects.filter(protocolo__isnull=False, resultado__isnull=True):
            lote.processar()
    print 'Thread finalizada!'


def execute(command):
    print command
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("localhost", 9999))
    s.sendall('%s\n' % command)
    out = s.recv(1024)
    print out
    return json.loads(out)








