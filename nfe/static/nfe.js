function admin__login(){
    $("#id_username").mask("000.000.000-00", {clearIfNotMatch: true});
}

function view__nfe__notafiscal(){
    function checar_situacao(){
        if($('#panel-dados-da-sefaz dd').length>0 && $('#panel-dados-da-sefaz dd')[1].innerHTML.length < 10) {
            setTimeout("$('#panel-dados-da-sefaz').load(document.location.href+' #panel-dados-da-sefaz')", 10000)
            setTimeout(checar_situacao, 20000);
        }
    }
    checar_situacao();
}
