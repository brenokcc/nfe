# -*- coding: utf-8 -*-
from django import template
register = template.Library()


@register.filter
def remover_pontuacao(numero):
    return numero.replace('.', '').replace('-', '').replace('/', '').replace('(', '').replace(')', '').replace(' ', '')

@register.filter
def decimal(numero):
    return unicode(numero)


