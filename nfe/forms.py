# -*- coding: utf-8 -*-
from djangoplus import forms


class UploadCertificadoForm(forms.Form):

    arquivo = forms.FileField(label=u'Arquivo')
    senha = forms.PasswordField(label=u'Senha')

    class Meta:
        title = u'Upload Certificado'



