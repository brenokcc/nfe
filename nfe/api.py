# -*- coding: utf-8 -*-
import urllib2, urllib, json
import base64


class NfeAPI(object):

    def __init__(self, token=None):
        self.token = token

    def _(self, data):
        data['token'] = self.token
        url = 'http://localhost:8000/nfe/webservice/'
        return json.loads(urllib2.urlopen(url=url, data=urllib.urlencode(data)).read() or '{}')

    def registrar(self, cnpj, certificado, senha):
        self.token = self._(dict(cnpj=cnpj, certificado=base64.b64encode(open(certificado, 'rb').read()), senha=senha, method='registrar'))['token']
        return self.token

    def enviar(self, xml):
        return self._(dict(xml=xml, method='enviar'))['chave']

    def cancelar(self, chave, motivo):
        return self._(dict(chave=chave, motivo=motivo, method='cancelar'))

    def baixar(self, chave):
        return self._(dict(chave=chave, method='baixar'))['xml']

    def consultar(self, *chaves):
        return self._(dict(chaves=' '.join(chaves), method='consultar'))

    def cadastro(self, cnpj):
        return self._(dict(cnpj=cnpj, method='cadastro'))

if __name__ == "__main__":
    api = NfeAPI('09432a22249879493e6becb77d4b767c13f5635c')
    print api.cadastro('09.302.478/0001-40')
    # xml = open('/Users/breno/Documents/Workspace/nfe/media/nfe/3/1.xml', 'rb').read()
    # print api.registrar('09.302.478/0001-40', '/Users/breno/Documents/Workspace/nfe/media/nfe/1/autoplac.pfx', 'autoplac')
    # chave = '24161109302478000140550010000000241902564230'#api.enviar(xml)
    # print chave
    # print api.baixar(chave)[0:90]
    # print api.consultar(chave)
    # print api.cancelar(chave, 'Teste de cancelamento de nota fiscal')
    #print api.consultar(chave)