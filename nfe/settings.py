# -*- coding: utf-8 -*-
from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DROPBOX_TOKEN = '' # disponível em https://www.dropbox.com/developers/apps
DROPBOX_LOCALDIR = MEDIA_ROOT
DROPBOX_REMOTEDIR = '/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME,
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if not exists(join(BASE_DIR, 'logs')):
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')

DROPBOX_TOKEN = ''
BACKUP_FILES = ['media', 'sqlite.db']
TEMPLATES[0]['OPTIONS']['builtins'].append('%s.templatefilters' % PROJECT_NAME)
EXTRA_CSS = ['/static/nfe.css']
EXTRA_JS = ['/static/nfe.js']

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)
